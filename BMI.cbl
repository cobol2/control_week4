       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. NAPHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT PIC 99(3). 
       01  HEIGHT PIC 99(3).
       01  RESULT PIC 99(2)V99.
      
       PROCEDURE DIVISION. 
       Begin.
           DISPLAY "Enter Weight(kg.): " 
              WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY "Enter Height(cm.): "
              WITH NO ADVANCING 
           ACCEPT HEIGHT
           COMPUTE RESULT = WEIGHT / ((HEIGHT / 100) * (HEIGHT / 100))

           DISPLAY "BMI Score = " RESULT

           IF RESULT < 18.5 THEN
              DISPLAY "UNDERWEIGHT"
           END-IF

           IF RESULT >= 18.5 AND < 24.9 THEN
              DISPLAY "NORMAL"
           END-IF

           IF RESULT >= 25 AND < 29.9 THEN
              DISPLAY "OVERWEIGHT"
           END-IF

           IF RESULT > 30 THEN
              DISPLAY "OBESE"
           END-IF
           .
